const url = 'mongodb://127.0.0.1:27017/cybeletech';
/**database connector */
const mongoose = require('mongoose');
mongoose.connect(url, { useNewUrlParser: true });

// define Schema

const MeteoSchema = mongoose.Schema({
  dateCreate: Date,
  parameter: String,
  coordinates: [
    {
      lat: Number,
      lon: Number,
      dates: [{ date: Date, value: String }],
    },
  ],
});
// compile schema to model
export const Meteo = mongoose.model('Meteo', MeteoSchema, 'meteo');

const db = mongoose.connection;
db.once('open', (_) => {
  console.log('Database connected:', url);
});
db.on('error', (err) => {
  console.error('connection error:', err);
});

/**axios */
const username = 'free_phamnguyen';
const password = 'evrMGWgNzX657';
const token = Buffer.from(`${username}:${password}`, 'utf8').toString('base64');
const axios = require('axios');
export const axiosObject = axios.create({
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Basic ${token}`,
  },
});
/**variables to get API data dynamically */
const moment = require('moment'); // require
const FORMAT_DATE = 'YYYY-MM-DDThh:mm:ssZ';
const currentDate = moment(new Date()).format(FORMAT_DATE);
const rangeInDay = 5;
const stepInDay = 1;
const formatExp = 'json';
const maxLat = 51;
const minLat = 42;
const maxLon = 8;
const minLon = -4;
const URL_API = `https://api.meteomatics.com/${currentDate}P${rangeInDay}D:P${stepInDay}D/t_2m:C,relative_humidity_2m:p,dew_point_2m:C,solar_power:MW/`; //47.412164,9.340652/json

const getDataMeteor = async (url) => {
  try {
    const response = await axiosObject.get(url);
    return response.data;
  } catch (error) {
    console.error(error);
  }
};
/**construire le url avec une date, , latitude, longitude et un */
const constructUrlApi = (lat, lon) => {
  return `${URL_API}${lat},${lon}/${formatExp}`;
};
/**Persist Data in Collection */
const persistDataInDb = (json) => {
  console.log('Begining to persist ', json);
  json.data.map((obj) => {
    const item = new Meteo({
      dateCreate: new Date(),
      parameter: obj.parameter,
      coordinates: obj.coordinates,
    });
    item.save((err, doc) => {
      if (err) return console.error(err);
      console.log(doc.parameter + ' saved to  collection.');
    });
  });
};
/**Ici , on récupère les json depuis API avec un url généré (date, fourchette , latitude, longitude) */
const generateDataFromAPI = async (urlGenerate) => {
  const json = await getDataMeteor(urlGenerate);
  persistDataInDb(json);
  console.log('data', moment(new Date()).format('hh:mm:ss'));
  await sleep(8000);
};

const formatNumberWithTwoDigit = (num) => {
  //return new Number(num.toFixed(1));
  return Math.floor(num);
};
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
const STEP_LATLON = 1;
async function load() {
  let nbUrl = 0;
  let i = minLat;
  while (i <= maxLat) {
    let j = minLon;
    while (j <= maxLon) {
      const urlConstruct = constructUrlApi(
        formatNumberWithTwoDigit(i),
        formatNumberWithTwoDigit(j)
      );
      console.log(urlConstruct);
      await generateDataFromAPI(urlConstruct);
      nbUrl++;
      j = j + STEP_LATLON;
    }
    i = i + STEP_LATLON;
  }
  console.log(nbUrl);
}
exports.load = load;
