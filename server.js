import express from 'express';
import { Meteo } from './dataLoad';
import cors from 'cors';
const app = express();
app.use(express.json());
app.use(cors());

/**app listen */
app.listen(3001, function () {
  console.log('listening on 3001');
});
/**request mapping */
app.get('/', async (req, res) => {
  return res.send('Welcome to Meteor Backend');
});

/*this url is the only reception for this project*/
app.post('/query', async (req, res) => {
  const params = req.body;
  console.log('parameters', params);
  const list = Meteo.findOne(params, (err, docs) => {
    console.log('result', docs);
    return res.send(JSON.parse(JSON.stringify(docs)));
  });

  // res.send(list);
});
